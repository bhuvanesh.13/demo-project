from webbrowser import Chrome
from behave import given,when,then
import time
import unittest
from selenium import webdriver
import sys
import Excel.Excelutils
from selenium.webdriver.support.select import Select
from PageObjects.test_IOGP_LoginPage import IOGPLoginPage
from PageObjects.test_IOGP_Observations import IOGP_ObservationsPage
sys.path.append("C://Users//arunm//OneDrive//PycharmProjects//PenchemmBDD_Automation")
chromeDriverExe = 'utilities\chromedriver.exe'
#email added successfully.close issue also
#checking the Trigger 
#processing noctua
 


@given(u'Launch Penchemm URL')
def step_Launch_Penchemm_URL(context):
    path="C://Users//Administrator//Desktop//chromedriver_win32//chromedriver.exe"
    context.driver=webdriver.Chrome(executable_path=path)
    context.driver.maximize_window()
    context.driver.get("https://safety-demo.detectpl.com/login/signin")
    time.sleep(2)

 

@given(u'User enters username "<vinay.qa@detecttechnologies.com>"')
def step_Enter_username(context):
    context.driver.find_element_by_xpath("//input[@id='username']").send_keys("vinay.qa@detecttechnologies.com")

 

@then(u'User enters password "<detect@123>"')
def step_I_enter_password(context):
    context.driver.find_element_by_xpath("// input[@id = 'password']").send_keys("detect@123")
    logintest = IOGPLoginPage(context.driver)
    logintest.Login_met()

 


@given(u'User Navigates to unit level dashboard by clicking on the arrow mark of unit')
def step_UserNavigates_to_unit_level_dashboard_by_clicking_on_the_arrow_mark_of_unit(context):
    logintest = IOGPLoginPage(context.driver)
    logintest.Click_OSBL()
    time.sleep(2)
    logintest.Click_observationLink()

 

@when(u'User clicks on the Hk or W@H Icon and navigate to the pages')
def step_User_clicks_on_the_Hk_or_WH_Icon_and_navigate_to_the_pages(context):
    logintest = IOGPLoginPage(context.driver)
    logintest.Click_observationLink()

 

@then(u'verify whether the videos is available in the observation table')
def step_verify_whether_the_videos_is_available_in_the_observation_table(context):
    logintest = IOGPLoginPage(context.driver)
    logintest.Verify_Video()

 

@given(u'User Navigates to OSBL Unit')
def step_User_Navigates_to_OSBL_Unit(context):
    logintest = IOGPLoginPage(context.driver)
    logintest.Click_OSBL()
    time.sleep(2)
    logintest.Click_observationLink()

 

@when(u'Click and play and of the available videos')
def step_Click_and_play_and_of_the_available_videos(context):
    logintest = IOGPLoginPage(context.driver)
    logintest.PlayObservationVideo()

 

@then(u'Verify the Video plays the same in a separate modal along with the observations and recommendations')
def step_Verify_the_Video_plays_the_same_in_a_separate_modal_along_with_the_observations_and_recommendations(context):
    print("Need to check vinay")

 

@when(u'user click on the video the download button should be enable.')
def step_impl(context):
    Observations_Page=IOGP_ObservationsPage(context.driver)
    Observations_Page.VideoDownload_Button()

 

@then(u'Verify whether the video is downloadable or not')
def step_impl(context):
    print("Need to check vinay")

 

@when(u'The videos are available in the observation table')
def step_impl(context):
    logintest = IOGPLoginPage(context.driver)
    logintest.Verify_Video()

 

@then(u'Verify The video has displayed only as a thumbnail and play button to be disabled in thumbnail')
def step_impl(context):
    print("Need to check vinay")

 

@then(u'verify an icon to indicate that the thumbnail is an image')
def step_impl(context):
    print("Need to check vinay")

 

@then(u'Verify the Zoom in and Zoom out on the video while video playing')
def step_impl(context):
    logintest = IOGPLoginPage(context.driver)
    logintest.ZoominAndOut()

 

@then(u'verify an icon to indicate that the thumbnail is an video')
def step_impl(context):
    print("Check with vinay")

 

@then(u'verify In Tile view, the user will be able to view the Image modal format.')
def step_impl(context):
    logintest = IOGPLoginPage(context.driver)
    logintest.TileView_ToggleButton()

 

@when(u'click on view all button')
def step_impl(context):
    print("check with vinay")

 

@then(u'Verify should open the all images in a slider view')
def step_impl(context):
    print("check with vinay")

 

@then(u'Verify Button to display a list of images with the fault counts to be provided')
def step_impl(context):
    print("check with vinay")

 

@then(u'Verify the user is able to search filter to search the images or videos')
def step_impl(context):
    logintest = IOGPLoginPage(context.driver)
    logintest.search_Icon()

 

@then(u'Verify the user is able to click on image or video in the observation table')
def step_impl(context):
    logintest = IOGPLoginPage(context.driver)
    logintest.Click_ObservationImage()

 

@when(u'User clicks on edit button')
def step_impl(context):
    Observations_Page = IOGP_ObservationsPage(context.driver)
    Observations_Page.Edit_Observation()

 

@when(u'User clicks on submit button')
def step_impl(context):
    Observations_Page = IOGP_ObservationsPage(context.driver)
    Observations_Page.Submit_Observation()

 

@then(u'Verify after editing the observation or recommendation the user should be able to submit to the server')
def step_impl(context):
    print("check with vinay")

 

@then(u'User clicks on delete button')
def step_impl(context):
    Observations_Page = IOGP_ObservationsPage(context.driver)
    Observations_Page.Delete_Observation()

 

@when(u'User clicks on open close drop down button')
def step_impl(context):
    Observations_Page = IOGP_ObservationsPage(context.driver)
    Observations_Page.Close_Observation()

 

@when(u'User clicks on close button')
def step_impl(context):
    print("Unable to close Access not provided")

 

@then(u'Verify the authorized user only able to close the observation or recommendation')
def step_impl(context):
    print("Unable to close Access not provided")

 

@when(u'User opens close drop down button in the observation table')
def step_impl(context):
    print("Unable to open Access not provided")

 

@then(u'Verify Remarks is enable or not in the observation table')
def step_impl(context):
    Observations_Page = IOGP_ObservationsPage(context.driver)
    Observations_Page.Verify_Remarks()

 

 

 

 

 

 

 